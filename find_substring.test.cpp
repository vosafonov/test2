#include <gtest/gtest.h>
#include "find_substring.h"

TEST(find_substring, usage)
{
    // поиск с пустыми строками
    auto res = find_substring("", "");
    ASSERT_EQ(0, res);

    res = find_substring("abcd", "");
    ASSERT_EQ(0, res);

    res = find_substring("", "abcd");
    ASSERT_EQ(std::string::npos, res);

    // поиск в начале строки
    res = find_substring("abcde", "ab");
    ASSERT_EQ(0, res);

    res = find_substring("abcde", "a");
    ASSERT_EQ(0, res);

    // поиск в середине строки 
    res = find_substring("abcde", "c");
    ASSERT_EQ(2, res);

    // поиск в конце строки
    res = find_substring("abcde", "de");
    ASSERT_EQ(3, res);

    res = find_substring("abcde", "e");
    ASSERT_EQ(4, res);

    // поиск строки целиком
    res = find_substring("qwerty", "qwerty");
    ASSERT_EQ(0, res);

    // поиск отсутствующей строки
    res = find_substring("qwerty", "qwertq");
    ASSERT_EQ(std::string::npos, res);

    res = find_substring("qwerty", "abcde");
    ASSERT_EQ(std::string::npos, res);

    // поиск повторяющейся строки
    res = find_substring("_ab_ab", "ab");
    ASSERT_EQ(1, res);
}
