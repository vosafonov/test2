#!/bin/bash

cd build
make -j4 && GTEST_COLOR=1 ctest --verbose
cd ..
