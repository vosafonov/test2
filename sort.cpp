#include "sort.h"
#include <algorithm>

void sort(std::vector<int> &vec)
{
    std::sort(vec.begin(), vec.end());
}
